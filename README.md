# Libre Space Manifesto #

This repository contains the [Libre Space Manifesto](https://manifesto.libre.space) website.

## Translations ##

Help us translate the Manifesto to your language in [Zanata](https://translate.zanata.org/project/view/lsf-manifesto?dswid=7998).

## License ##

[![license](https://img.shields.io/badge/license-CC%20BY--SA%204.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202019-Libre%20Space%20Foundation-6672D8.svg)](https://libre.space/)
