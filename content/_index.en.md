*Space is humanity’s future.*

It is humanity’s opportunity to explore, develop, use, and thrive differently. A way to ensure the *longevity, sustainability, openness, equality* of those efforts for all humanity. 

For this, we pledge to adhere to the following:

Principles
----

1. All people shall have the right to explore and use outer space for the benefit and in the interests of all humanity.

2. Exploration and use of outer space shall be carried out collaboratively and cooperatively.

3. Outer space shall be used exclusively for peaceful purposes.

4. Profit shall not be the driving force for space exploration.

5. All people shall have access to outer space, space technologies, and space data.

***

To achieve those principles, we need to adhere to the following:

Pillars
----

1.  **Open Source**

    All technologies developed for outer space shall be published and licensed using open source licenses.

2.  **Open Data**

    All data related to and produced in outer space shall be freely accessed, used and built upon by anyone, anywhere, and shall be shared and managed according to the principles above.

3.  **Open Development**

    All technologies for outer space shall be developed in a transparent, legible, documented, testable, modular, and efficient way.

4.  **Open Governance**

    All technologies for outer space shall be governed in a participatory, collaborative, direct, and distributed way.


***


Support
----

The Manifesto is a bedrock for development in the space age. By adding you or your organization, you can show your support for its principles and pillars.

To join as an organization or individual, sign below.

{{< form-sign action="https://airform.io/manifesto@libre.space" >}}